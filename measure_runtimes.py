import argparse
import string
import numpy as np
import os.path
import time

import torch
import torchvision

from models import GestureClassifier
from dataset import SequentialGestureDataSet
from transforms import GroupNormalize, GroupScale, GroupCenterCrop
from util import splits_LOSO, splits_LOUO, splits_LOUO_NP, gestures_SU, gestures_NP, gestures_KT


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


def measure(args):

    if not torch.cuda.is_available():
        print("Cannot find GPU.")
        return
    device_gpu = torch.device("cuda:0")

    if len([t for t in string.Formatter().parse(args.data_path)]) > 1:
        args.data_path = args.data_path.format(args.task)
    if len([t for t in string.Formatter().parse(args.video_lists_dir)]) > 1:
        args.video_lists_dir = args.video_lists_dir.format(args.task)
    if len([t for t in string.Formatter().parse(args.transcriptions_dir)]) > 1:
        args.transcriptions_dir = args.transcriptions_dir.format(args.task)

    gesture_ids = None
    if args.task == "Suturing":
        gesture_ids = gestures_SU
    elif args.task == "Needle_Passing":
        gesture_ids = gestures_NP
    elif args.task == "Knot_Tying":
        gesture_ids = gestures_KT

    splits = None
    if args.eval_scheme == 'LOSO':
        splits = splits_LOSO
    elif args.eval_scheme == 'LOUO':
        if args.task == "Needle_Passing":
            splits = splits_LOUO_NP
        else:
            splits = splits_LOUO

    num_class = len(gesture_ids)
    net = GestureClassifier(args.arch, num_class, args.modality, dropout=0,
                            snippet_length=args.snippet_length, input_size=args.input_size,
                            pretrained_model=None, bootstrap_from_2D=None, use_resnet_shortcut_type_B=False)

    # === load data... (using any available data) ===

    normalize = GroupNormalize(net.input_mean, net.input_std)
    test_augmentation = torchvision.transforms.Compose([GroupScale(int(net.scale_size)),
                                                        GroupCenterCrop(net.crop_size)])

    videos = list()
    lists_dir = os.path.join(args.video_lists_dir, args.eval_scheme)
    video_lists = list(map(lambda x: os.path.join(lists_dir, x), splits))
    for list_file in video_lists:
        videos.extend([(x.strip().split(',')[0], x.strip().split(',')[1]) for x in open(list_file)])
    test_datasets = list()
    for video in videos:
        data_set = SequentialGestureDataSet(args.data_path, args.transcriptions_dir, gesture_ids, video[0], int(video[1]),
                                            snippet_length=net.snippet_length,
                                            video_sampling_step=args.video_sampling_step,
                                            snippet_sampling_step=args.snippet_sampling_step,
                                            modality=args.modality, image_tmpl=args.image_tmpl,
                                            video_suffix=args.video_suffix, return_3D_tensor=net.is_3D_architecture,
                                            transform=test_augmentation, normalize=normalize, load_to_RAM=False)
        test_datasets.append(data_set)
    test_loader = torch.utils.data.DataLoader(torch.utils.data.ConcatDataset(test_datasets), batch_size=1,
                                              shuffle=False, num_workers=1)

    # === evaluate... ===

    torch.manual_seed(0)
    np.random.seed(0)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    n_measure = 2500
    n_warmup = 250

    net = net.to(device_gpu)
    net.eval()

    start = torch.cuda.Event(enable_timing=True)
    end = torch.cuda.Event(enable_timing=True)

    data_loading_times = []
    inference_times = []
    overall_start = time.time()
    data_start = time.time()
    with torch.no_grad():
        for i, batch in enumerate(test_loader):

            if i >= n_measure + n_warmup:
                break

            data, target = batch
            data_end = time.time()

            torch.cuda.synchronize()
            start.record()
            data = data.to(device_gpu)
            out = net(data)
            torch.cuda.synchronize()
            end.record()

            torch.cuda.synchronize()
            inference_times.append(start.elapsed_time(end))

            data_loading_times.append(data_end - data_start)
            data_start = time.time()

    overall_end = time.time()

    print("Overall duration: {} sec".format(overall_end - overall_start))
    print("Performed {} measurements, total time elapsed: {:.4f} sec"
          .format(len(inference_times), np.sum(inference_times) / 1000 + np.sum(data_loading_times)))
    print("Measured data loading times (s): {:.4f} +- {:.4f}; [{:.4f} - {:.4f}]"
          .format(np.mean(data_loading_times[n_warmup:]),
                  np.std(data_loading_times[n_warmup:]),
                  np.min(data_loading_times[n_warmup:]),
                  np.max(data_loading_times[n_warmup:])))
    print("Measured inference times (ms): {:.4f} +- {:.4f}; [{:.4f} - {:.4f}]"
          .format(np.mean(inference_times[n_warmup:]),
                  np.std(inference_times[n_warmup:]),
                  np.min(inference_times[n_warmup:]),
                  np.max(inference_times[n_warmup:])))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Measure inference times of surgical gesture classifiers.")
    parser.register('type', 'bool', str2bool)

    parser.add_argument('--data_path', type=str, default="?",
                        help="Path to data folder, which contains the extracted images for each video. "
                             "One subfolder per video.")
    parser.add_argument('--transcriptions_dir', type=str, default="?",
                        help="Path to folder containing the transcription files (gesture annotations). One file per video.")
    parser.add_argument('--arch', type=str, default="3D-ResNet-18", choices=['3D-ResNet-18', 'resnet18'],
                        help="Network architecture.")
    parser.add_argument('--use_resnet_shortcut_type_B', type='bool', default=False,
                        help="Whether to use shortcut connections of type B.")
    parser.add_argument('--snippet_length', type=int, default=16,
                        help="Number of frames constituting one video snippet.")
    parser.add_argument('--input_size', type=int, default=224, help="Target size (width/ height) of each frame.")
    parser.add_argument('--task', type=str, choices=['Suturing', 'Needle_Passing', 'Knot_Tying'], default='Suturing',
                        help="JIGSAWS task to evaluate.")
    parser.add_argument('--eval_scheme', type=str, choices=['LOSO', 'LOUO'], default='LOUO',
                        help="Cross-validation scheme to use: Leave one supertrial out (LOSO) or Leave one user out (LOUO).")
    parser.add_argument('--modality', type=str, default='RGB', choices=['RGB', 'Flow'], help="Used input modality.")
    parser.add_argument('--video_lists_dir', type=str, default="./Splits/{}/",
                        help="Path to directory containing information about each video in the form of video list files. "
                             "One subfolder per evaluation scheme, one file per evaluation fold.")
    parser.add_argument('--video_sampling_step', type=int, default=6,
                        help="Specifies at which temporal resolution each video will be evaluated. "
                             "More specifically, we will sample one video snippet every <video_sampling_step>th frame.")
    parser.add_argument('--snippet_sampling_step', type=int, default=6,
                        help="Specifies at which temporal resolution the video snippets will be created (by taking every "
                             "<snippet_sampling_step>th video frame). ")

    args = parser.parse_args()
    args.video_suffix = "_capture2"
    args.image_tmpl = 'img_{:05d}.jpg'
    if args.modality == 'Flow':
        args.image_tmpl = 'flow_{}_{:05d}.jpg'

    measure(args)
