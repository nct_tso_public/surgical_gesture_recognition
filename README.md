# Using 3D Convolutional Neural Networks to Learn Spatiotemporal Features for Automatic Surgical Gesture Recognition in Video

PyTorch implementation of video-based surgical gesture recognition using 3D convolutional neural networks. 
We propose to use a modified [3D ResNet-18](http://openaccess.thecvf.com/content_ICCV_2017_workshops/papers/w44/Hara_Learning_Spatio-Temporal_Features_ICCV_2017_paper.pdf) to predict dense gesture labels for RGB video input. Details can be found in our [paper](https://arxiv.org/abs/1907.11454).

This implementation is based on open source code published by Kensho Hara in the [3D-ResNets-PyTorch](https://github.com/kenshohara/3D-ResNets-PyTorch) repository.

## Code

### How to start
Simply clone this repository:
```bash
cd <the directory where the repo shall live>
git clone https://gitlab.com/nct_tso_public/surgical_gesture_recognition.git
```
In the following, we use `CODE_DIR` to refer to the absolute path to the code.

Check if you have all required Python packages installed. Our code depends on
> torch torchvision numpy scipy pillow opencv-python datetime sklearn numba matplotlib seaborn pandas

Experiments were run using Python 3.6 (Python 3.5 should also work fine) and  [PyTorch 1.0.0](https://pytorch.org) with Cuda 9.2.

### Data preparation
Download the JIGSAWS dataset from [here](https://cirl.lcsr.jhu.edu/research/hmm/datasets/jigsaws_release/) and unzip it. You will obtain one folder per surgical task (`Suturing`, `Needle_Passing`, and `Knot_Tying`). We use `DATA_DIR` to refer to the absolute path to the *parent* of these folders.

To extract video frames and to pre-calculate optical flow, we used the code provided by [Limin Wang](http://wanglimin.github.io/), [Yuanjun Xiong](http://yjxiong.me/), and colleagues. You can do the same by executing the following steps:

- Download and build the *dense_flow* code:
Run `git clone --recursive http://github.com/yjxiong/dense_flow` and follow the [install](https://github.com/yjxiong/dense_flow) instructions. 
We use `DF_BUILD_DIR` to refer to the absolute path to your *dense_flow* build folder, i.e., the folder containing the binary `extract_gpu` after successful installation.
Note that you will have to install LibZip and OpenCV to compile the code.
    - We used OpenCV 3.4.
    - OpenCV must be built with CUDA support (`-D WITH_CUDA=ON`) and with extra modules (`-D OPENCV_EXTRA_MODULES_PATH=/<path>/<to>/<your>/<opencv_contrib>/`).
    - When building the *dense_flow* code, you can specify a custom location of your OpenCV library by running `OpenCV_DIR=/<path>/<to>/<your>/<opencv_dir>/  cmake ..` (instead of simply `cmake ..`)

- Run the script `extract_frames.sh`:
```bash
cd <CODE_DIR>
bash extract_frames.sh <DF_BUILD_DIR> <DATA_DIR>
```
This will extract frames at 5 fps from all *suturing* videos in the JIGSAWS dataset. The script skips videos ending with `capture_1.avi` because we only consider the video of the *right* camera.

Optionally, you can specify the parameters `step_size`, `num_gpu`, and `jobs_per_gpu` as 3rd, 4th, and 5th command line arguments. Here, `step_size` specifies at which temporal resolution the frames are extracted (namely at `<original fps>/<step_size>` fps), `num_gpu >= 1` specifies how many GPUs (at least one) are used, and `jobs_per_gpu` specifies how many videos will be processed in parallel on each GPU. For example, `bash extract_frames.sh <DF_BUILD_DIR> <DATA_DIR> 6 2 8` will extract frames at 30 fps / `<step_size>` = 5 fps using two GPUs and eight workers per GPU. Per default, we use `num_gpu = 1`, `jobs_per_gpu = 4`, and `step_size = 6`.

Finally, the data folder structure will look like this:
```
<DATA_DIR>
	Suturing
		video
			Suturing_B001_capture1.avi
			Suturing_B001_capture2.avi
			...
		transcriptions
			Suturing_B001.txt
			Suturing_B002.txt
			...
		(other JIGSAWS specific files and folders)
		frames
			Suturing_B001_capture2
				flow_x_00001.jpg
				flow_x_00002.jpg
				...
				flow_y_00001.jpg
				...
				img_00001.jpg
				...
			Suturing_B002_capture2
				...
```

### Train a model

#### Quick start

- To obtain the weights of the [Kinetics](https://deepmind.com/research/open-source/kinetics)-pretrained *3D ResNet-18* model, download the file `resnet-18-kinetics.pth` that is made available [here](https://github.com/kenshohara/3D-ResNets-PyTorch#pre-trained-models) (you need to follow the first link).

- Create the video list files, which are required by our training and evaluation scripts, by running:
```bash
cd <CODE_DIR>
python3 create_video_files.py --data_dir <DATA_DIR>
```
- Now, the following command will train a model for surgical gesture recognition on the JIGSAWS suturing task, starting from the Kinetics-pretrained *3D ResNet-18*. 
```bash
python3 train.py --exp <EXP> --split <SPLIT> --pretrain_path "/<your>/<path>/<to>/resnet-18-kinetics.pth" --data_path "<DATA_DIR>/Suturing/frames" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --out <OUT_DIR> 
```
The command line parameter `--split` specifies which LOUO cross-validation fold is left out from the training data.
Results, e.g., model files, will be written to `<OUT_DIR>/<EXP>_<current date>/LOUO/<SPLIT>/<current time>`.
Note that we require you to specify a name `EXP` for the experiment so that you can identify the trained models at a later time.

You can set defaults, e.g.,  for `--data_path`, `--transcriptions_dir`, and `--out`, in the file `train_opts.py`. 
Run `python3 train.py -h` to get a complete list of all command line parameters that can be specified.

#### More experiments

You can repeat the other experiments described in our paper as follows:

- Train a baseline *2D ResNet-18* model:
```bash
python3 train.py --exp <2D_EXP> --split <SPLIT> --arch resnet18 --snippet_length 1 --data_path "<DATA_DIR>/Suturing/frames" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --out <OUT_DIR> 
```
- Train a 3D CNN for surgical gesture recognition after bootstrapping weights from a trained *2D ResNet-18* model:
```bash
python3 train.py --exp <EXP> --split <SPLIT> --use_resnet_shortcut_type_B True --bootstrap_from_2D True --pretrain_path "<OUT_DIR>/<2D_EXP>_<date>" --data_path "<DATA_DIR>/Suturing/frames" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --out <OUT_DIR> 
```
In this case, the script expects to find the 2D models trained during a previous experiment at `<OUT_DIR>/<2D_EXP>_<date>/LOUO/<SPLIT>/<some timestamp>/`.

### Evaluate trained models

After training a model for every cross-validation fold, you can evaluate the experiment. 
To test the 3D CNN that was initialized with Kinetics-pretrained weights, you can run:
```bash
python3 test.py --exp <EXP>_<date> --data_path "<DATA_DIR>/Suturing/frames" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --model_dir <OUT_DIR> 
```
Here, `date` is the timestamp (current date) generated for the experiment at training time. The script expects to find the trained models at `<OUT_DIR>/<EXP>_<date>/LOUO/<SPLIT>/<some timestamp>/model_<no>.pth.tar`. By default, `no` is set to 249, which is the number of the final models saved after 250 epochs of training. You can evaluate models saved at earlier points during training by setting the command line parameter `--model_no`.

The script computes the surgical gesture estimates for every video in the dataset, using the model that hasn't seen the video at training time. The predictions are compared against the ground truth labels to compute the evaluation metrics (accuracy, average F1 score, edit score, and segmental F1 score). Results are saved as a python dictionary at  `<OUT_DIR>/Eval/LOUO/5Hz/plain/<EXP>_<date>/<model_no>.pth.tar`.

If the dense gesture predictions shall be aggregated over time to obtain final gesture estimates (*sliding window*), just add `--sliding_window True` to the command. In this case, results will be saved at `<OUT_DIR>/Eval/LOUO/5Hz/window/<EXP>_<date>/<model_no>.pth.tar`.

The other experiments can be evaluated analogously:

- Evaluate the *2D ResNet-18* baseline:
```bash
python3 test.py --exp <EXP>_<date> --arch resnet18 --snippet_length 1 --data_path "<DATA_DIR>/Suturing/frames" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --model_dir <OUT_DIR>
```
- Evaluate the 3D CNN that was initialized with weights bootstrapped from a 2D model:
```bash
python3 test.py --exp <EXP>_<date> --use_resnet_shortcut_type_B True --data_path "<DATA_DIR>/Suturing/frames" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --model_dir <OUT_DIR> [--sliding_window True]
```

Run `python3 test.py -h` to get a complete list of all command line parameters that can be specified.

#### Evaluation at 2 Hz or 10 Hz

You can opt to extract the snippets considered for evaluation at 2 Hz or 10 Hz (instead of 5 Hz) from the videos. To do this, you need to add `--video_sampling_step 15` (2 Hz) or `--video_sampling_step 3` (10 Hz) to the command. Note that this won't alter the temporal structure within one video snippet. 

To extract video snippets at frequencies other than 5 Hz, the script requires access to the video frames extracted at the full temporal resolution of 30 fps. To extract frames at 30 fps, you can run:
```bash
bash extract_frames.sh <DF_BUILD_DIR> <DATA_DIR> 1 <num_gpu> <jobs_per_gpu> "frames_30Hz"
```
Here, the 6th command line argument achieves that the extracted frames are written to `<DATA_DIR>/Suturing/frames_30Hz` (instead of messing with the previously created `frames` folder).
Now, you can set `--data_path "<DATA_DIR>/Suturing/frames_30Hz"` in the command, for example
```bash
python3 test.py --exp <EXP>_<date> --data_path "<DATA_DIR>/Suturing/frames_30Hz" --transcriptions_dir "<DATA_DIR>/Suturing/transcriptions" --model_dir <OUT_DIR> --video_sampling_step 3 [--sliding_window True]
```

## How to cite

If you use parts of the code in your own research, please cite:
	
	@InProceedings{funke2019,
	    author="Funke, Isabel and Bodenstedt, Sebastian and Oehme, Florian and von Bechtolsheim, Felix and Weitz, J{\"u}rgen and Speidel, Stefanie",
	    title="Using {3D} Convolutional Neural Networks to Learn Spatiotemporal Features for Automatic Surgical Gesture Recognition in Video",
	    booktitle="Medical Image Computing and Computer Assisted Intervention -- MICCAI 2019",
	    year="2019",
	    pages="467--475",
	    editor="Shen, Dinggang and Liu, Tianming and Peters, Terry M. and Staib, Lawrence H. and Essert, Caroline and Zhou, Sean and Yap, Pew-Thian and Khan, Ali",
	    series="Lecture Notes in Computer Science",
  	    volume="11768",
	    publisher="Springer International Publishing",
	    address="Cham",
	    doi="10.1007/978-3-030-32254-0\_52"
	}

This work was carried out at the National Center for Tumor Diseases (NCT) Dresden, [Department of Translational Surgical Oncology](https://www.nct-dresden.de/tso.html).
