# Copyright (C) 2019  National Center of Tumor Diseases (NCT) Dresden, Division of Translational Surgical Oncology

import argparse
import os.path
import numpy as np
import torch


def eval_runs(args):
    eval_type = "plain"
    if args.sliding_window:
        eval_type = "window"
        if args.look_ahead is not None:
            eval_type += "_" + str(args.look_ahead)
    eval_freq = 30 // args.video_sampling_step

    base_dir = os.path.join(args.model_dir, "Eval", args.eval_scheme, "{}Hz".format(eval_freq), eval_type)
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    epochs = args.model_no
    if epochs is None:
        # find epochs to evaluate
        epochs = []
        for file in [f for f in os.listdir(os.path.join(base_dir, args.exp_runs[0])) if not f.startswith('.')]:
            if file.endswith("pth.tar"):
                epochs.append(int(file.split('.')[0]))
        epochs = sorted(epochs)

    eval_metrics = ['acc', 'avg_f1', 'edit', 'f1_10', 'f1_25', 'f1_50']

    results = {}
    for epoch in epochs:
        averaged_results = {}
        video_level_results = {}
        for metric in eval_metrics:
            averaged_results[metric] = []
            video_level_results[metric] = []
        conf_mat = None
        for exp in args.exp_runs:
            eval_file = os.path.join(base_dir, exp, "{}.pth.tar".format(epoch))
            if not os.path.exists(eval_file):
                print("Cannot find {}".format(eval_file))
                print("Consider running test.py for experiment {}".format(exp))
                continue
            eval_results = torch.load(eval_file)
            for key in eval_results:
                if key == 'overall':
                    overall_results = eval_results[key]  # averaged over all videos
                    for m in eval_metrics:
                        averaged_results[m].append(overall_results[m][0])
                    if conf_mat is None:
                        conf_mat = overall_results['conf_mat']
                    else:
                        conf_mat = conf_mat + overall_results['conf_mat']
                else:  # key = video_id
                    video_results = eval_results[key]
                    for m in eval_metrics:
                        video_level_results[m].append(video_results[m])
        for m in eval_metrics:
            averaged_results[m] = (np.mean(averaged_results[m]), np.std(averaged_results[m]))
            video_level_results[m] = (np.mean(video_level_results[m]), np.std(video_level_results[m]))
        results[epoch] = {"exp_level_avg": averaged_results,
                          "video_level_avg": video_level_results,
                          "conf_mat": conf_mat}
    torch.save(results, os.path.join(base_dir, "{}.pth.tar".format(args.exp_group_name)))

    results_file = os.path.join(base_dir, "{}.dat".format(args.exp_group_name))
    data_log = open(results_file, "w")
    data_log.write("===== average over individual videos =====" + os.linesep)
    data_log.write("epoch\tacc (+/- std)\tavg_f1 (+/- std)\tedit (+/- std)\tf1@10 (+/- std)" + os.linesep)
    for epoch in epochs:
        acc = results[epoch]['video_level_avg']['acc']
        avg_f1 = results[epoch]['video_level_avg']['avg_f1']
        edit = results[epoch]['video_level_avg']['edit']
        f1_10 = results[epoch]['video_level_avg']['f1_10']
        msg = "{}\t{:.4f}+/-{:.4f}\t{:.4f}+/-{:.4f}\t{:.4f}+/-{:.4f}\t{:.4f}+/-{:.4f}"\
            .format(epoch, acc[0], acc[1], avg_f1[0], avg_f1[1], edit[0], edit[1], f1_10[0], f1_10[1])
        print(msg)
        data_log.write(msg + os.linesep)

    data_log.write(os.linesep + os.linesep + os.linesep)

    data_log.write("===== average over several runs =====" + os.linesep)
    data_log.write("epoch\tacc (+/- std)\tavg_f1 (+/- std)\tedit (+/- std)\tf1@10 (+/- std)" + os.linesep)
    for epoch in epochs:
        acc = results[epoch]['exp_level_avg']['acc']
        avg_f1 = results[epoch]['exp_level_avg']['avg_f1']
        edit = results[epoch]['exp_level_avg']['edit']
        f1_10 = results[epoch]['exp_level_avg']['f1_10']
        msg = "{}\t{:.4f}+/-{:.4f}\t{:.4f}+/-{:.4f}\t{:.4f}+/-{:.4f}\t{:.4f}+/-{:.4f}"\
            .format(epoch, acc[0], acc[1], avg_f1[0], avg_f1[1], edit[0], edit[1], f1_10[0], f1_10[1])
        print(msg)
        data_log.write(msg + os.linesep)

    data_log.close()


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.register('type', 'bool', str2bool)

    parser.add_argument('exp_group_name', type=str, help="Summarizing name for the experiment. "
                                                         "Will be used to name the results file.")
    parser.add_argument('exp_runs', type=str, nargs="+",
                        help="Name of all experiments (including auto-generated timestamp) for which results shall be "
                             "aggregated.")
    parser.add_argument('--model_dir', type=str, required=True,
                        help="Path to the folder where the results of the relevant experiment(s) are stored. "
                             "Usually identical to <out> as specified during training.")
    parser.add_argument('--model_no', type=int, nargs="+", default=[249],
                        help="Defines the models to evaluate by specifying their model numbers. Will perform one "
                             "evaluation run per model number. The number of a model corresponds to the number of "
                             "epochs for which the model has been trained - 1.")
    parser.add_argument('--eval_scheme', type=str, choices=['LOSO', 'LOUO'], default='LOUO',
                        help="Cross-validation scheme to use: Leave one supertrial out (LOSO) or "
                             "Leave one user out (LOUO).")
    parser.add_argument('--video_sampling_step', type=int, default=6,
                        help="Specifies at which temporal resolution each video will be evaluated. "
                             "More specifically, we will sample one video snippet every <video_sampling_step>th frame.")
    parser.add_argument('--sliding_window', type='bool', default=False,
                        help="Whether to accumulate predictions over time.")
    parser.add_argument('--look_ahead', type=int, choices=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                        help="If <sliding_window> is true, predictions for future snippets are considered only if "
                             "they are at most <look_ahead> steps away.")

    args = parser.parse_args()
    eval_runs(args)
